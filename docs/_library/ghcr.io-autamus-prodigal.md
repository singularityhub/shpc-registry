---
layout: container
name:  "ghcr.io/autamus/prodigal"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/ghcr.io/autamus/prodigal/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/ghcr.io/autamus/prodigal/container.yaml"
updated_at: "2022-08-18 21:47:25.624430"
latest: "2.6.3"
container_url: "https://github.com/orgs/autamus/packages/container/package/prodigal"
aliases:
 - "prodigal"
versions:
 - "2.6.3"
 - "latest"
description: "Prodigal Gene Prediction Software"
config: {"docker": "ghcr.io/autamus/prodigal", "url": "https://github.com/orgs/autamus/packages/container/package/prodigal", "maintainer": "@vsoch", "description": "Prodigal Gene Prediction Software", "latest": {"2.6.3": "sha256:13874a0e9131e41eed2ce3c4c2a51a209f06d0d90f661fc81b5aa322abae6775"}, "tags": {"2.6.3": "sha256:13874a0e9131e41eed2ce3c4c2a51a209f06d0d90f661fc81b5aa322abae6775", "latest": "sha256:13874a0e9131e41eed2ce3c4c2a51a209f06d0d90f661fc81b5aa322abae6775"}, "aliases": {"prodigal": "/opt/view/prodigal"}}
---

This module is a singularity container wrapper for ghcr.io/autamus/prodigal.
Prodigal Gene Prediction Software
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install ghcr.io/autamus/prodigal
```

Or a specific version:

```bash
$ shpc install ghcr.io/autamus/prodigal:2.6.3
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load ghcr.io/autamus/prodigal/2.6.3
$ module help ghcr.io/autamus/prodigal/2.6.3
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### prodigal-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### prodigal-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### prodigal-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### prodigal-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### prodigal-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### prodigal-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### prodigal
       
```bash
$ singularity exec <container> /opt/view/prodigal
$ podman run --it --rm --entrypoint /opt/view/prodigal   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /opt/view/prodigal   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)