---
layout: container
name:  "biocontainers/tabix"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/biocontainers/tabix/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/biocontainers/tabix/container.yaml"
updated_at: "2022-08-18 21:47:05.167786"
latest: "v1.9-11-deb_cv1"
container_url: "https://hub.docker.com/r/biocontainers/tabix"
aliases:
 - "tabix"
versions:
 - "v1.9-11-deb_cv1"
description: "Fast retrieval of sequence features from generic TAB-delimited files."
config: {"docker": "biocontainers/tabix", "latest": {"v1.9-11-deb_cv1": "sha256:7e093436d00c01cf6ad7b285680bf1657f9fcb692cc083c972e5df5a7e951f49"}, "tags": {"v1.9-11-deb_cv1": "sha256:7e093436d00c01cf6ad7b285680bf1657f9fcb692cc083c972e5df5a7e951f49"}, "filter": ["v*"], "maintainer": "@vsoch", "url": "https://hub.docker.com/r/biocontainers/tabix", "description": "Fast retrieval of sequence features from generic TAB-delimited files.", "aliases": {"tabix": "/usr/bin/tabix"}}
---

This module is a singularity container wrapper for biocontainers/tabix.
Fast retrieval of sequence features from generic TAB-delimited files.
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install biocontainers/tabix
```

Or a specific version:

```bash
$ shpc install biocontainers/tabix:v1.9-11-deb_cv1
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load biocontainers/tabix/v1.9-11-deb_cv1
$ module help biocontainers/tabix/v1.9-11-deb_cv1
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### tabix-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### tabix-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### tabix-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### tabix-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### tabix-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### tabix-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### tabix
       
```bash
$ singularity exec <container> /usr/bin/tabix
$ podman run --it --rm --entrypoint /usr/bin/tabix   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/bin/tabix   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)