---
layout: container
name:  "quay.io/biocontainers/bamtools"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/quay.io/biocontainers/bamtools/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/quay.io/biocontainers/bamtools/container.yaml"
updated_at: "2022-08-18 21:46:50.363308"
latest: "2.5.2--hd03093a_0"
container_url: "https://quay.io/repository/biocontainers/bamtools"
aliases:
 - "bamtools"
versions:
 - "2.5.1--he860b03_4"
 - "2.5.1--h9a82719_9"
 - "2.5.1--hd03093a_10"
 - "2.5.2--hd03093a_0"
description: "C++ API and command-line toolkit for working with BAM data."
config: {"docker": "quay.io/biocontainers/bamtools", "url": "https://quay.io/repository/biocontainers/bamtools", "maintainer": "@marcodelapierre", "description": "C++ API and command-line toolkit for working with BAM data.", "latest": {"2.5.2--hd03093a_0": "sha256:599e39895bf24cd8cbbccda2a09609dc54435523298147d872267d94dfa19804"}, "tags": {"2.5.1--he860b03_4": "sha256:40bc7f10591d9ed34c75a789e286fec80cd1e5d642b10274d09b09f494e14831", "2.5.1--h9a82719_9": "sha256:1df72d906a4db1f47d71516a8f026eecd614271f9c4c8e5e8ccde9210fcde227", "2.5.1--hd03093a_10": "sha256:a5b2a2e6c9d080f478adb5fa5406b6a2a808489d999911c1a9752a227ccd8c6b", "2.5.2--hd03093a_0": "sha256:599e39895bf24cd8cbbccda2a09609dc54435523298147d872267d94dfa19804"}, "aliases": {"bamtools": "/usr/local/bin/bamtools"}}
---

This module is a singularity container wrapper for quay.io/biocontainers/bamtools.
C++ API and command-line toolkit for working with BAM data.
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install quay.io/biocontainers/bamtools
```

Or a specific version:

```bash
$ shpc install quay.io/biocontainers/bamtools:2.5.1--he860b03_4
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load quay.io/biocontainers/bamtools/2.5.1--he860b03_4
$ module help quay.io/biocontainers/bamtools/2.5.1--he860b03_4
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### bamtools-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### bamtools-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### bamtools-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### bamtools-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### bamtools-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### bamtools-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### bamtools
       
```bash
$ singularity exec <container> /usr/local/bin/bamtools
$ podman run --it --rm --entrypoint /usr/local/bin/bamtools   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bamtools   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)