---
layout: container
name:  "quay.io/biocontainers/bbmap"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/quay.io/biocontainers/bbmap/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/quay.io/biocontainers/bbmap/container.yaml"
updated_at: "2022-08-18 21:46:52.667797"
latest: "38.96--h5c4e2a8_0"
container_url: "https://quay.io/repository/biocontainers/bbmap"
aliases:
 - "a_sample_mt.sh"
 - "addadapters.sh"
 - "addssu.sh"
 - "adjusthomopolymers.sh"
 - "alltoall.sh"
 - "analyzeaccession.sh"
 - "analyzegenes.sh"
 - "analyzesketchresults.sh"
 - "applyvariants.sh"
 - "bbcms.sh"
 - "bbcountunique.sh"
 - "bbduk.sh"
 - "bbest.sh"
 - "bbfakereads.sh"
 - "bbmap.sh"
 - "bbmapskimmer.sh"
 - "bbmask.sh"
 - "bbmerge-auto.sh"
 - "bbmerge.sh"
 - "bbnorm.sh"
 - "bbrealign.sh"
 - "bbrename.sh"
 - "bbsketch.sh"
 - "bbsplit.sh"
 - "bbsplitpairs.sh"
 - "bbstats.sh"
 - "bbversion.sh"
 - "bbwrap.sh"
 - "bloomfilter.sh"
 - "build_env_setup.sh"
 - "calcmem.sh"
 - "calctruequality.sh"
 - "callgenes.sh"
 - "callpeaks.sh"
 - "callvariants.sh"
 - "callvariants2.sh"
 - "clumpify.sh"
 - "commonkmers.sh"
 - "comparegff.sh"
 - "comparesketch.sh"
 - "comparessu.sh"
 - "comparevcf.sh"
 - "conda_build.sh"
 - "consect.sh"
 - "consensus.sh"
 - "countbarcodes.sh"
 - "countgc.sh"
 - "countsharedlines.sh"
 - "crossblock.sh"
 - "crosscontaminate.sh"
 - "cutgff.sh"
 - "cutprimers.sh"
 - "decontaminate.sh"
 - "dedupe.sh"
 - "dedupe2.sh"
 - "dedupebymapping.sh"
 - "demuxbyname.sh"
 - "diskbench.sh"
 - "estherfilter.sh"
 - "explodetree.sh"
 - "fetchproks.sh"
 - "filterassemblysummary.sh"
 - "filterbarcodes.sh"
 - "filterbycoverage.sh"
 - "filterbyname.sh"
 - "filterbysequence.sh"
 - "filterbytaxa.sh"
 - "filterbytile.sh"
 - "filterlines.sh"
 - "filterqc.sh"
 - "filtersam.sh"
 - "filtersilva.sh"
 - "filtersubs.sh"
 - "filtervcf.sh"
 - "fixgaps.sh"
 - "fungalrelease.sh"
 - "fuse.sh"
 - "gbff2gff.sh"
 - "getreads.sh"
 - "gi2ancestors.sh"
 - "gi2taxid.sh"
 - "gitable.sh"
 - "grademerge.sh"
 - "gradesam.sh"
 - "icecreamfinder.sh"
 - "icecreamgrader.sh"
 - "icecreammaker.sh"
 - "idmatrix.sh"
 - "idtree.sh"
 - "invertkey.sh"
 - "kapastats.sh"
 - "kcompress.sh"
 - "keepbestcopy.sh"
 - "khist.sh"
 - "kmercountexact.sh"
 - "kmercountmulti.sh"
 - "kmercoverage.sh"
 - "kmerfilterset.sh"
 - "kmerlimit.sh"
 - "kmerlimit2.sh"
 - "kmerposition.sh"
 - "kmutate.sh"
 - "lilypad.sh"
 - "loadreads.sh"
 - "loglog.sh"
 - "makechimeras.sh"
 - "makecontaminatedgenomes.sh"
 - "makepolymers.sh"
 - "mapPacBio.sh"
 - "matrixtocolumns.sh"
 - "mergeOTUs.sh"
 - "mergebarcodes.sh"
 - "mergepgm.sh"
 - "mergeribo.sh"
 - "mergesam.sh"
 - "mergesketch.sh"
 - "mergesorted.sh"
 - "msa.sh"
 - "mutate.sh"
 - "muxbyname.sh"
 - "partition.sh"
 - "phylip2fasta.sh"
 - "pileup.sh"
 - "plotflowcell.sh"
 - "plotgc.sh"
 - "postfilter.sh"
 - "printtime.sh"
 - "processfrag.sh"
 - "processhi-c.sh"
 - "processspeed.sh"
 - "randomgenome.sh"
 - "randomreads.sh"
 - "readlength.sh"
 - "readqc.sh"
 - "reducesilva.sh"
 - "reformat.sh"
 - "reformatpb.sh"
 - "removebadbarcodes.sh"
 - "removecatdogmousehuman.sh"
 - "removehuman.sh"
 - "removehuman2.sh"
 - "removemicrobes.sh"
 - "removesmartbell.sh"
 - "rename.sh"
 - "renameimg.sh"
 - "repair.sh"
 - "replaceheaders.sh"
 - "representative.sh"
 - "rqcfilter.sh"
 - "rqcfilter2.sh"
 - "runhmm.sh"
 - "samtoroc.sh"
 - "seal.sh"
 - "sendsketch.sh"
 - "shred.sh"
 - "shrinkaccession.sh"
 - "shuffle.sh"
 - "shuffle2.sh"
 - "sketch.sh"
 - "sketchblacklist.sh"
 - "sketchblacklist2.sh"
 - "sortbyname.sh"
 - "splitbytaxa.sh"
 - "splitnextera.sh"
 - "splitribo.sh"
 - "splitsam.sh"
 - "splitsam4way.sh"
 - "splitsam6way.sh"
 - "stats.sh"
 - "statswrapper.sh"
 - "streamsam.sh"
 - "subsketch.sh"
 - "summarizecontam.sh"
 - "summarizecoverage.sh"
 - "summarizecrossblock.sh"
 - "summarizemerge.sh"
 - "summarizequast.sh"
 - "summarizescafstats.sh"
 - "summarizeseal.sh"
 - "summarizesketch.sh"
 - "synthmda.sh"
 - "tadpipe.sh"
 - "tadpole.sh"
 - "tadwrapper.sh"
 - "taxonomy.sh"
 - "taxserver.sh"
 - "taxsize.sh"
 - "taxtree.sh"
 - "testfilesystem.sh"
 - "testformat.sh"
 - "testformat2.sh"
 - "tetramerfreq.sh"
 - "textfile.sh"
 - "translate6frames.sh"
 - "unicode2ascii.sh"
 - "unzip.sh"
 - "vcf2gff.sh"
 - "webcheck.sh"
versions:
 - "38.89--h1296035_0"
 - "38.90--he522d1c_3"
 - "38.91--he522d1c_1"
 - "38.92--he522d1c_0"
 - "38.93--he522d1c_0"
 - "38.96--h5c4e2a8_0"
description: "BBMap is a short read aligner, as well as various other bioinformatic tools."
config: {"docker": "quay.io/biocontainers/bbmap", "url": "https://quay.io/repository/biocontainers/bbmap", "maintainer": "@marcodelapierre", "description": "BBMap is a short read aligner, as well as various other bioinformatic tools.", "latest": {"38.96--h5c4e2a8_0": "sha256:1bc2b5f07fd506ee2cce882a1f8b7b40abffe4e860a89bd87e6599e09dee827c"}, "tags": {"38.89--h1296035_0": "sha256:acc26b0e54b1323c2db287fabb8bffecda94e766b6759dfdddc7472687b52f33", "38.90--he522d1c_3": "sha256:e85733071f68bc84959aa97155f9d13b87fdfd17f41703fe861c057a56dec2b0", "38.91--he522d1c_1": "sha256:5679f2c146844662be023769146fa787ea101f3dd833ade36aaa0bb533c9939a", "38.92--he522d1c_0": "sha256:103f3a1ec4144933c583da1f8f9bfc7447468e0150c9591d4cc1aff8f98830b9", "38.93--he522d1c_0": "sha256:c171d975ea9b1d4232af2e3608927b5ec83807608263d4e12428964b2f99e4dc", "38.96--h5c4e2a8_0": "sha256:1bc2b5f07fd506ee2cce882a1f8b7b40abffe4e860a89bd87e6599e09dee827c"}, "aliases": {"a_sample_mt.sh": "/usr/local/bin/a_sample_mt.sh", "addadapters.sh": "/usr/local/bin/addadapters.sh", "addssu.sh": "/usr/local/bin/addssu.sh", "adjusthomopolymers.sh": "/usr/local/bin/adjusthomopolymers.sh", "alltoall.sh": "/usr/local/bin/alltoall.sh", "analyzeaccession.sh": "/usr/local/bin/analyzeaccession.sh", "analyzegenes.sh": "/usr/local/bin/analyzegenes.sh", "analyzesketchresults.sh": "/usr/local/bin/analyzesketchresults.sh", "applyvariants.sh": "/usr/local/bin/applyvariants.sh", "bbcms.sh": "/usr/local/bin/bbcms.sh", "bbcountunique.sh": "/usr/local/bin/bbcountunique.sh", "bbduk.sh": "/usr/local/bin/bbduk.sh", "bbest.sh": "/usr/local/bin/bbest.sh", "bbfakereads.sh": "/usr/local/bin/bbfakereads.sh", "bbmap.sh": "/usr/local/bin/bbmap.sh", "bbmapskimmer.sh": "/usr/local/bin/bbmapskimmer.sh", "bbmask.sh": "/usr/local/bin/bbmask.sh", "bbmerge-auto.sh": "/usr/local/bin/bbmerge-auto.sh", "bbmerge.sh": "/usr/local/bin/bbmerge.sh", "bbnorm.sh": "/usr/local/bin/bbnorm.sh", "bbrealign.sh": "/usr/local/bin/bbrealign.sh", "bbrename.sh": "/usr/local/bin/bbrename.sh", "bbsketch.sh": "/usr/local/bin/bbsketch.sh", "bbsplit.sh": "/usr/local/bin/bbsplit.sh", "bbsplitpairs.sh": "/usr/local/bin/bbsplitpairs.sh", "bbstats.sh": "/usr/local/bin/bbstats.sh", "bbversion.sh": "/usr/local/bin/bbversion.sh", "bbwrap.sh": "/usr/local/bin/bbwrap.sh", "bloomfilter.sh": "/usr/local/bin/bloomfilter.sh", "build_env_setup.sh": "/usr/local/bin/build_env_setup.sh", "calcmem.sh": "/usr/local/bin/calcmem.sh", "calctruequality.sh": "/usr/local/bin/calctruequality.sh", "callgenes.sh": "/usr/local/bin/callgenes.sh", "callpeaks.sh": "/usr/local/bin/callpeaks.sh", "callvariants.sh": "/usr/local/bin/callvariants.sh", "callvariants2.sh": "/usr/local/bin/callvariants2.sh", "clumpify.sh": "/usr/local/bin/clumpify.sh", "commonkmers.sh": "/usr/local/bin/commonkmers.sh", "comparegff.sh": "/usr/local/bin/comparegff.sh", "comparesketch.sh": "/usr/local/bin/comparesketch.sh", "comparessu.sh": "/usr/local/bin/comparessu.sh", "comparevcf.sh": "/usr/local/bin/comparevcf.sh", "conda_build.sh": "/usr/local/bin/conda_build.sh", "consect.sh": "/usr/local/bin/consect.sh", "consensus.sh": "/usr/local/bin/consensus.sh", "countbarcodes.sh": "/usr/local/bin/countbarcodes.sh", "countgc.sh": "/usr/local/bin/countgc.sh", "countsharedlines.sh": "/usr/local/bin/countsharedlines.sh", "crossblock.sh": "/usr/local/bin/crossblock.sh", "crosscontaminate.sh": "/usr/local/bin/crosscontaminate.sh", "cutgff.sh": "/usr/local/bin/cutgff.sh", "cutprimers.sh": "/usr/local/bin/cutprimers.sh", "decontaminate.sh": "/usr/local/bin/decontaminate.sh", "dedupe.sh": "/usr/local/bin/dedupe.sh", "dedupe2.sh": "/usr/local/bin/dedupe2.sh", "dedupebymapping.sh": "/usr/local/bin/dedupebymapping.sh", "demuxbyname.sh": "/usr/local/bin/demuxbyname.sh", "diskbench.sh": "/usr/local/bin/diskbench.sh", "estherfilter.sh": "/usr/local/bin/estherfilter.sh", "explodetree.sh": "/usr/local/bin/explodetree.sh", "fetchproks.sh": "/usr/local/bin/fetchproks.sh", "filterassemblysummary.sh": "/usr/local/bin/filterassemblysummary.sh", "filterbarcodes.sh": "/usr/local/bin/filterbarcodes.sh", "filterbycoverage.sh": "/usr/local/bin/filterbycoverage.sh", "filterbyname.sh": "/usr/local/bin/filterbyname.sh", "filterbysequence.sh": "/usr/local/bin/filterbysequence.sh", "filterbytaxa.sh": "/usr/local/bin/filterbytaxa.sh", "filterbytile.sh": "/usr/local/bin/filterbytile.sh", "filterlines.sh": "/usr/local/bin/filterlines.sh", "filterqc.sh": "/usr/local/bin/filterqc.sh", "filtersam.sh": "/usr/local/bin/filtersam.sh", "filtersilva.sh": "/usr/local/bin/filtersilva.sh", "filtersubs.sh": "/usr/local/bin/filtersubs.sh", "filtervcf.sh": "/usr/local/bin/filtervcf.sh", "fixgaps.sh": "/usr/local/bin/fixgaps.sh", "fungalrelease.sh": "/usr/local/bin/fungalrelease.sh", "fuse.sh": "/usr/local/bin/fuse.sh", "gbff2gff.sh": "/usr/local/bin/gbff2gff.sh", "getreads.sh": "/usr/local/bin/getreads.sh", "gi2ancestors.sh": "/usr/local/bin/gi2ancestors.sh", "gi2taxid.sh": "/usr/local/bin/gi2taxid.sh", "gitable.sh": "/usr/local/bin/gitable.sh", "grademerge.sh": "/usr/local/bin/grademerge.sh", "gradesam.sh": "/usr/local/bin/gradesam.sh", "icecreamfinder.sh": "/usr/local/bin/icecreamfinder.sh", "icecreamgrader.sh": "/usr/local/bin/icecreamgrader.sh", "icecreammaker.sh": "/usr/local/bin/icecreammaker.sh", "idmatrix.sh": "/usr/local/bin/idmatrix.sh", "idtree.sh": "/usr/local/bin/idtree.sh", "invertkey.sh": "/usr/local/bin/invertkey.sh", "kapastats.sh": "/usr/local/bin/kapastats.sh", "kcompress.sh": "/usr/local/bin/kcompress.sh", "keepbestcopy.sh": "/usr/local/bin/keepbestcopy.sh", "khist.sh": "/usr/local/bin/khist.sh", "kmercountexact.sh": "/usr/local/bin/kmercountexact.sh", "kmercountmulti.sh": "/usr/local/bin/kmercountmulti.sh", "kmercoverage.sh": "/usr/local/bin/kmercoverage.sh", "kmerfilterset.sh": "/usr/local/bin/kmerfilterset.sh", "kmerlimit.sh": "/usr/local/bin/kmerlimit.sh", "kmerlimit2.sh": "/usr/local/bin/kmerlimit2.sh", "kmerposition.sh": "/usr/local/bin/kmerposition.sh", "kmutate.sh": "/usr/local/bin/kmutate.sh", "lilypad.sh": "/usr/local/bin/lilypad.sh", "loadreads.sh": "/usr/local/bin/loadreads.sh", "loglog.sh": "/usr/local/bin/loglog.sh", "makechimeras.sh": "/usr/local/bin/makechimeras.sh", "makecontaminatedgenomes.sh": "/usr/local/bin/makecontaminatedgenomes.sh", "makepolymers.sh": "/usr/local/bin/makepolymers.sh", "mapPacBio.sh": "/usr/local/bin/mapPacBio.sh", "matrixtocolumns.sh": "/usr/local/bin/matrixtocolumns.sh", "mergeOTUs.sh": "/usr/local/bin/mergeOTUs.sh", "mergebarcodes.sh": "/usr/local/bin/mergebarcodes.sh", "mergepgm.sh": "/usr/local/bin/mergepgm.sh", "mergeribo.sh": "/usr/local/bin/mergeribo.sh", "mergesam.sh": "/usr/local/bin/mergesam.sh", "mergesketch.sh": "/usr/local/bin/mergesketch.sh", "mergesorted.sh": "/usr/local/bin/mergesorted.sh", "msa.sh": "/usr/local/bin/msa.sh", "mutate.sh": "/usr/local/bin/mutate.sh", "muxbyname.sh": "/usr/local/bin/muxbyname.sh", "partition.sh": "/usr/local/bin/partition.sh", "phylip2fasta.sh": "/usr/local/bin/phylip2fasta.sh", "pileup.sh": "/usr/local/bin/pileup.sh", "plotflowcell.sh": "/usr/local/bin/plotflowcell.sh", "plotgc.sh": "/usr/local/bin/plotgc.sh", "postfilter.sh": "/usr/local/bin/postfilter.sh", "printtime.sh": "/usr/local/bin/printtime.sh", "processfrag.sh": "/usr/local/bin/processfrag.sh", "processhi-c.sh": "/usr/local/bin/processhi-c.sh", "processspeed.sh": "/usr/local/bin/processspeed.sh", "randomgenome.sh": "/usr/local/bin/randomgenome.sh", "randomreads.sh": "/usr/local/bin/randomreads.sh", "readlength.sh": "/usr/local/bin/readlength.sh", "readqc.sh": "/usr/local/bin/readqc.sh", "reducesilva.sh": "/usr/local/bin/reducesilva.sh", "reformat.sh": "/usr/local/bin/reformat.sh", "reformatpb.sh": "/usr/local/bin/reformatpb.sh", "removebadbarcodes.sh": "/usr/local/bin/removebadbarcodes.sh", "removecatdogmousehuman.sh": "/usr/local/bin/removecatdogmousehuman.sh", "removehuman.sh": "/usr/local/bin/removehuman.sh", "removehuman2.sh": "/usr/local/bin/removehuman2.sh", "removemicrobes.sh": "/usr/local/bin/removemicrobes.sh", "removesmartbell.sh": "/usr/local/bin/removesmartbell.sh", "rename.sh": "/usr/local/bin/rename.sh", "renameimg.sh": "/usr/local/bin/renameimg.sh", "repair.sh": "/usr/local/bin/repair.sh", "replaceheaders.sh": "/usr/local/bin/replaceheaders.sh", "representative.sh": "/usr/local/bin/representative.sh", "rqcfilter.sh": "/usr/local/bin/rqcfilter.sh", "rqcfilter2.sh": "/usr/local/bin/rqcfilter2.sh", "runhmm.sh": "/usr/local/bin/runhmm.sh", "samtoroc.sh": "/usr/local/bin/samtoroc.sh", "seal.sh": "/usr/local/bin/seal.sh", "sendsketch.sh": "/usr/local/bin/sendsketch.sh", "shred.sh": "/usr/local/bin/shred.sh", "shrinkaccession.sh": "/usr/local/bin/shrinkaccession.sh", "shuffle.sh": "/usr/local/bin/shuffle.sh", "shuffle2.sh": "/usr/local/bin/shuffle2.sh", "sketch.sh": "/usr/local/bin/sketch.sh", "sketchblacklist.sh": "/usr/local/bin/sketchblacklist.sh", "sketchblacklist2.sh": "/usr/local/bin/sketchblacklist2.sh", "sortbyname.sh": "/usr/local/bin/sortbyname.sh", "splitbytaxa.sh": "/usr/local/bin/splitbytaxa.sh", "splitnextera.sh": "/usr/local/bin/splitnextera.sh", "splitribo.sh": "/usr/local/bin/splitribo.sh", "splitsam.sh": "/usr/local/bin/splitsam.sh", "splitsam4way.sh": "/usr/local/bin/splitsam4way.sh", "splitsam6way.sh": "/usr/local/bin/splitsam6way.sh", "stats.sh": "/usr/local/bin/stats.sh", "statswrapper.sh": "/usr/local/bin/statswrapper.sh", "streamsam.sh": "/usr/local/bin/streamsam.sh", "subsketch.sh": "/usr/local/bin/subsketch.sh", "summarizecontam.sh": "/usr/local/bin/summarizecontam.sh", "summarizecoverage.sh": "/usr/local/bin/summarizecoverage.sh", "summarizecrossblock.sh": "/usr/local/bin/summarizecrossblock.sh", "summarizemerge.sh": "/usr/local/bin/summarizemerge.sh", "summarizequast.sh": "/usr/local/bin/summarizequast.sh", "summarizescafstats.sh": "/usr/local/bin/summarizescafstats.sh", "summarizeseal.sh": "/usr/local/bin/summarizeseal.sh", "summarizesketch.sh": "/usr/local/bin/summarizesketch.sh", "synthmda.sh": "/usr/local/bin/synthmda.sh", "tadpipe.sh": "/usr/local/bin/tadpipe.sh", "tadpole.sh": "/usr/local/bin/tadpole.sh", "tadwrapper.sh": "/usr/local/bin/tadwrapper.sh", "taxonomy.sh": "/usr/local/bin/taxonomy.sh", "taxserver.sh": "/usr/local/bin/taxserver.sh", "taxsize.sh": "/usr/local/bin/taxsize.sh", "taxtree.sh": "/usr/local/bin/taxtree.sh", "testfilesystem.sh": "/usr/local/bin/testfilesystem.sh", "testformat.sh": "/usr/local/bin/testformat.sh", "testformat2.sh": "/usr/local/bin/testformat2.sh", "tetramerfreq.sh": "/usr/local/bin/tetramerfreq.sh", "textfile.sh": "/usr/local/bin/textfile.sh", "translate6frames.sh": "/usr/local/bin/translate6frames.sh", "unicode2ascii.sh": "/usr/local/bin/unicode2ascii.sh", "unzip.sh": "/usr/local/bin/unzip.sh", "vcf2gff.sh": "/usr/local/bin/vcf2gff.sh", "webcheck.sh": "/usr/local/bin/webcheck.sh"}}
---

This module is a singularity container wrapper for quay.io/biocontainers/bbmap.
BBMap is a short read aligner, as well as various other bioinformatic tools.
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install quay.io/biocontainers/bbmap
```

Or a specific version:

```bash
$ shpc install quay.io/biocontainers/bbmap:38.89--h1296035_0
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load quay.io/biocontainers/bbmap/38.89--h1296035_0
$ module help quay.io/biocontainers/bbmap/38.89--h1296035_0
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### bbmap-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### bbmap-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### bbmap-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### bbmap-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### bbmap-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### bbmap-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### a_sample_mt.sh
       
```bash
$ singularity exec <container> /usr/local/bin/a_sample_mt.sh
$ podman run --it --rm --entrypoint /usr/local/bin/a_sample_mt.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/a_sample_mt.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### addadapters.sh
       
```bash
$ singularity exec <container> /usr/local/bin/addadapters.sh
$ podman run --it --rm --entrypoint /usr/local/bin/addadapters.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/addadapters.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### addssu.sh
       
```bash
$ singularity exec <container> /usr/local/bin/addssu.sh
$ podman run --it --rm --entrypoint /usr/local/bin/addssu.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/addssu.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### adjusthomopolymers.sh
       
```bash
$ singularity exec <container> /usr/local/bin/adjusthomopolymers.sh
$ podman run --it --rm --entrypoint /usr/local/bin/adjusthomopolymers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/adjusthomopolymers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### alltoall.sh
       
```bash
$ singularity exec <container> /usr/local/bin/alltoall.sh
$ podman run --it --rm --entrypoint /usr/local/bin/alltoall.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/alltoall.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### analyzeaccession.sh
       
```bash
$ singularity exec <container> /usr/local/bin/analyzeaccession.sh
$ podman run --it --rm --entrypoint /usr/local/bin/analyzeaccession.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/analyzeaccession.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### analyzegenes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/analyzegenes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/analyzegenes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/analyzegenes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### analyzesketchresults.sh
       
```bash
$ singularity exec <container> /usr/local/bin/analyzesketchresults.sh
$ podman run --it --rm --entrypoint /usr/local/bin/analyzesketchresults.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/analyzesketchresults.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### applyvariants.sh
       
```bash
$ singularity exec <container> /usr/local/bin/applyvariants.sh
$ podman run --it --rm --entrypoint /usr/local/bin/applyvariants.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/applyvariants.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbcms.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbcms.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbcms.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbcms.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbcountunique.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbcountunique.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbcountunique.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbcountunique.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbduk.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbduk.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbduk.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbduk.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbest.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbest.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbest.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbest.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbfakereads.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbfakereads.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbfakereads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbfakereads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbmap.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbmap.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbmap.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbmap.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbmapskimmer.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbmapskimmer.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbmapskimmer.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbmapskimmer.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbmask.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbmask.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbmask.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbmask.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbmerge-auto.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbmerge-auto.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbmerge-auto.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbmerge-auto.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbmerge.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbmerge.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbmerge.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbmerge.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbnorm.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbnorm.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbnorm.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbnorm.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbrealign.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbrealign.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbrealign.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbrealign.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbrename.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbrename.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbrename.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbrename.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbsketch.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbsketch.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbsketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbsketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbsplit.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbsplit.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbsplit.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbsplit.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbsplitpairs.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbsplitpairs.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbsplitpairs.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbsplitpairs.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbstats.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbstats.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbstats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbstats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbversion.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbversion.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbversion.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbversion.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bbwrap.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bbwrap.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bbwrap.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bbwrap.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### bloomfilter.sh
       
```bash
$ singularity exec <container> /usr/local/bin/bloomfilter.sh
$ podman run --it --rm --entrypoint /usr/local/bin/bloomfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bloomfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### build_env_setup.sh
       
```bash
$ singularity exec <container> /usr/local/bin/build_env_setup.sh
$ podman run --it --rm --entrypoint /usr/local/bin/build_env_setup.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/build_env_setup.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### calcmem.sh
       
```bash
$ singularity exec <container> /usr/local/bin/calcmem.sh
$ podman run --it --rm --entrypoint /usr/local/bin/calcmem.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/calcmem.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### calctruequality.sh
       
```bash
$ singularity exec <container> /usr/local/bin/calctruequality.sh
$ podman run --it --rm --entrypoint /usr/local/bin/calctruequality.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/calctruequality.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### callgenes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/callgenes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/callgenes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/callgenes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### callpeaks.sh
       
```bash
$ singularity exec <container> /usr/local/bin/callpeaks.sh
$ podman run --it --rm --entrypoint /usr/local/bin/callpeaks.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/callpeaks.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### callvariants.sh
       
```bash
$ singularity exec <container> /usr/local/bin/callvariants.sh
$ podman run --it --rm --entrypoint /usr/local/bin/callvariants.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/callvariants.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### callvariants2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/callvariants2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/callvariants2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/callvariants2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### clumpify.sh
       
```bash
$ singularity exec <container> /usr/local/bin/clumpify.sh
$ podman run --it --rm --entrypoint /usr/local/bin/clumpify.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/clumpify.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### commonkmers.sh
       
```bash
$ singularity exec <container> /usr/local/bin/commonkmers.sh
$ podman run --it --rm --entrypoint /usr/local/bin/commonkmers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/commonkmers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### comparegff.sh
       
```bash
$ singularity exec <container> /usr/local/bin/comparegff.sh
$ podman run --it --rm --entrypoint /usr/local/bin/comparegff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/comparegff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### comparesketch.sh
       
```bash
$ singularity exec <container> /usr/local/bin/comparesketch.sh
$ podman run --it --rm --entrypoint /usr/local/bin/comparesketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/comparesketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### comparessu.sh
       
```bash
$ singularity exec <container> /usr/local/bin/comparessu.sh
$ podman run --it --rm --entrypoint /usr/local/bin/comparessu.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/comparessu.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### comparevcf.sh
       
```bash
$ singularity exec <container> /usr/local/bin/comparevcf.sh
$ podman run --it --rm --entrypoint /usr/local/bin/comparevcf.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/comparevcf.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### conda_build.sh
       
```bash
$ singularity exec <container> /usr/local/bin/conda_build.sh
$ podman run --it --rm --entrypoint /usr/local/bin/conda_build.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/conda_build.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### consect.sh
       
```bash
$ singularity exec <container> /usr/local/bin/consect.sh
$ podman run --it --rm --entrypoint /usr/local/bin/consect.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/consect.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### consensus.sh
       
```bash
$ singularity exec <container> /usr/local/bin/consensus.sh
$ podman run --it --rm --entrypoint /usr/local/bin/consensus.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/consensus.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### countbarcodes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/countbarcodes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/countbarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/countbarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### countgc.sh
       
```bash
$ singularity exec <container> /usr/local/bin/countgc.sh
$ podman run --it --rm --entrypoint /usr/local/bin/countgc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/countgc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### countsharedlines.sh
       
```bash
$ singularity exec <container> /usr/local/bin/countsharedlines.sh
$ podman run --it --rm --entrypoint /usr/local/bin/countsharedlines.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/countsharedlines.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### crossblock.sh
       
```bash
$ singularity exec <container> /usr/local/bin/crossblock.sh
$ podman run --it --rm --entrypoint /usr/local/bin/crossblock.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/crossblock.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### crosscontaminate.sh
       
```bash
$ singularity exec <container> /usr/local/bin/crosscontaminate.sh
$ podman run --it --rm --entrypoint /usr/local/bin/crosscontaminate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/crosscontaminate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### cutgff.sh
       
```bash
$ singularity exec <container> /usr/local/bin/cutgff.sh
$ podman run --it --rm --entrypoint /usr/local/bin/cutgff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/cutgff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### cutprimers.sh
       
```bash
$ singularity exec <container> /usr/local/bin/cutprimers.sh
$ podman run --it --rm --entrypoint /usr/local/bin/cutprimers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/cutprimers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### decontaminate.sh
       
```bash
$ singularity exec <container> /usr/local/bin/decontaminate.sh
$ podman run --it --rm --entrypoint /usr/local/bin/decontaminate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/decontaminate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### dedupe.sh
       
```bash
$ singularity exec <container> /usr/local/bin/dedupe.sh
$ podman run --it --rm --entrypoint /usr/local/bin/dedupe.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/dedupe.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### dedupe2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/dedupe2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/dedupe2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/dedupe2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### dedupebymapping.sh
       
```bash
$ singularity exec <container> /usr/local/bin/dedupebymapping.sh
$ podman run --it --rm --entrypoint /usr/local/bin/dedupebymapping.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/dedupebymapping.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### demuxbyname.sh
       
```bash
$ singularity exec <container> /usr/local/bin/demuxbyname.sh
$ podman run --it --rm --entrypoint /usr/local/bin/demuxbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/demuxbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### diskbench.sh
       
```bash
$ singularity exec <container> /usr/local/bin/diskbench.sh
$ podman run --it --rm --entrypoint /usr/local/bin/diskbench.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/diskbench.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### estherfilter.sh
       
```bash
$ singularity exec <container> /usr/local/bin/estherfilter.sh
$ podman run --it --rm --entrypoint /usr/local/bin/estherfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/estherfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### explodetree.sh
       
```bash
$ singularity exec <container> /usr/local/bin/explodetree.sh
$ podman run --it --rm --entrypoint /usr/local/bin/explodetree.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/explodetree.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### fetchproks.sh
       
```bash
$ singularity exec <container> /usr/local/bin/fetchproks.sh
$ podman run --it --rm --entrypoint /usr/local/bin/fetchproks.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/fetchproks.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterassemblysummary.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterassemblysummary.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterassemblysummary.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterassemblysummary.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterbarcodes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterbarcodes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterbarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterbarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterbycoverage.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterbycoverage.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterbycoverage.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterbycoverage.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterbyname.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterbyname.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterbysequence.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterbysequence.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterbysequence.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterbysequence.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterbytaxa.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterbytaxa.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterbytaxa.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterbytaxa.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterbytile.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterbytile.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterbytile.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterbytile.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterlines.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterlines.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterlines.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterlines.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filterqc.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filterqc.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filterqc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filterqc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filtersam.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filtersam.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filtersam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filtersam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filtersilva.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filtersilva.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filtersilva.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filtersilva.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filtersubs.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filtersubs.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filtersubs.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filtersubs.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### filtervcf.sh
       
```bash
$ singularity exec <container> /usr/local/bin/filtervcf.sh
$ podman run --it --rm --entrypoint /usr/local/bin/filtervcf.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/filtervcf.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### fixgaps.sh
       
```bash
$ singularity exec <container> /usr/local/bin/fixgaps.sh
$ podman run --it --rm --entrypoint /usr/local/bin/fixgaps.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/fixgaps.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### fungalrelease.sh
       
```bash
$ singularity exec <container> /usr/local/bin/fungalrelease.sh
$ podman run --it --rm --entrypoint /usr/local/bin/fungalrelease.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/fungalrelease.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### fuse.sh
       
```bash
$ singularity exec <container> /usr/local/bin/fuse.sh
$ podman run --it --rm --entrypoint /usr/local/bin/fuse.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/fuse.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### gbff2gff.sh
       
```bash
$ singularity exec <container> /usr/local/bin/gbff2gff.sh
$ podman run --it --rm --entrypoint /usr/local/bin/gbff2gff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/gbff2gff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### getreads.sh
       
```bash
$ singularity exec <container> /usr/local/bin/getreads.sh
$ podman run --it --rm --entrypoint /usr/local/bin/getreads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/getreads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### gi2ancestors.sh
       
```bash
$ singularity exec <container> /usr/local/bin/gi2ancestors.sh
$ podman run --it --rm --entrypoint /usr/local/bin/gi2ancestors.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/gi2ancestors.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### gi2taxid.sh
       
```bash
$ singularity exec <container> /usr/local/bin/gi2taxid.sh
$ podman run --it --rm --entrypoint /usr/local/bin/gi2taxid.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/gi2taxid.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### gitable.sh
       
```bash
$ singularity exec <container> /usr/local/bin/gitable.sh
$ podman run --it --rm --entrypoint /usr/local/bin/gitable.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/gitable.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### grademerge.sh
       
```bash
$ singularity exec <container> /usr/local/bin/grademerge.sh
$ podman run --it --rm --entrypoint /usr/local/bin/grademerge.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/grademerge.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### gradesam.sh
       
```bash
$ singularity exec <container> /usr/local/bin/gradesam.sh
$ podman run --it --rm --entrypoint /usr/local/bin/gradesam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/gradesam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### icecreamfinder.sh
       
```bash
$ singularity exec <container> /usr/local/bin/icecreamfinder.sh
$ podman run --it --rm --entrypoint /usr/local/bin/icecreamfinder.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/icecreamfinder.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### icecreamgrader.sh
       
```bash
$ singularity exec <container> /usr/local/bin/icecreamgrader.sh
$ podman run --it --rm --entrypoint /usr/local/bin/icecreamgrader.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/icecreamgrader.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### icecreammaker.sh
       
```bash
$ singularity exec <container> /usr/local/bin/icecreammaker.sh
$ podman run --it --rm --entrypoint /usr/local/bin/icecreammaker.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/icecreammaker.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### idmatrix.sh
       
```bash
$ singularity exec <container> /usr/local/bin/idmatrix.sh
$ podman run --it --rm --entrypoint /usr/local/bin/idmatrix.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/idmatrix.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### idtree.sh
       
```bash
$ singularity exec <container> /usr/local/bin/idtree.sh
$ podman run --it --rm --entrypoint /usr/local/bin/idtree.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/idtree.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### invertkey.sh
       
```bash
$ singularity exec <container> /usr/local/bin/invertkey.sh
$ podman run --it --rm --entrypoint /usr/local/bin/invertkey.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/invertkey.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kapastats.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kapastats.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kapastats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kapastats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kcompress.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kcompress.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kcompress.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kcompress.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### keepbestcopy.sh
       
```bash
$ singularity exec <container> /usr/local/bin/keepbestcopy.sh
$ podman run --it --rm --entrypoint /usr/local/bin/keepbestcopy.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/keepbestcopy.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### khist.sh
       
```bash
$ singularity exec <container> /usr/local/bin/khist.sh
$ podman run --it --rm --entrypoint /usr/local/bin/khist.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/khist.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmercountexact.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmercountexact.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmercountexact.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmercountexact.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmercountmulti.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmercountmulti.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmercountmulti.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmercountmulti.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmercoverage.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmercoverage.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmercoverage.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmercoverage.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmerfilterset.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmerfilterset.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmerfilterset.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmerfilterset.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmerlimit.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmerlimit.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmerlimit.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmerlimit.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmerlimit2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmerlimit2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmerlimit2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmerlimit2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmerposition.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmerposition.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmerposition.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmerposition.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### kmutate.sh
       
```bash
$ singularity exec <container> /usr/local/bin/kmutate.sh
$ podman run --it --rm --entrypoint /usr/local/bin/kmutate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/kmutate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### lilypad.sh
       
```bash
$ singularity exec <container> /usr/local/bin/lilypad.sh
$ podman run --it --rm --entrypoint /usr/local/bin/lilypad.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/lilypad.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### loadreads.sh
       
```bash
$ singularity exec <container> /usr/local/bin/loadreads.sh
$ podman run --it --rm --entrypoint /usr/local/bin/loadreads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/loadreads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### loglog.sh
       
```bash
$ singularity exec <container> /usr/local/bin/loglog.sh
$ podman run --it --rm --entrypoint /usr/local/bin/loglog.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/loglog.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### makechimeras.sh
       
```bash
$ singularity exec <container> /usr/local/bin/makechimeras.sh
$ podman run --it --rm --entrypoint /usr/local/bin/makechimeras.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/makechimeras.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### makecontaminatedgenomes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/makecontaminatedgenomes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/makecontaminatedgenomes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/makecontaminatedgenomes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### makepolymers.sh
       
```bash
$ singularity exec <container> /usr/local/bin/makepolymers.sh
$ podman run --it --rm --entrypoint /usr/local/bin/makepolymers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/makepolymers.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mapPacBio.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mapPacBio.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mapPacBio.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mapPacBio.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### matrixtocolumns.sh
       
```bash
$ singularity exec <container> /usr/local/bin/matrixtocolumns.sh
$ podman run --it --rm --entrypoint /usr/local/bin/matrixtocolumns.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/matrixtocolumns.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mergeOTUs.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mergeOTUs.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mergeOTUs.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mergeOTUs.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mergebarcodes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mergebarcodes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mergebarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mergebarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mergepgm.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mergepgm.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mergepgm.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mergepgm.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mergeribo.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mergeribo.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mergeribo.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mergeribo.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mergesam.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mergesam.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mergesam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mergesam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mergesketch.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mergesketch.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mergesketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mergesketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mergesorted.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mergesorted.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mergesorted.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mergesorted.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### msa.sh
       
```bash
$ singularity exec <container> /usr/local/bin/msa.sh
$ podman run --it --rm --entrypoint /usr/local/bin/msa.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/msa.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### mutate.sh
       
```bash
$ singularity exec <container> /usr/local/bin/mutate.sh
$ podman run --it --rm --entrypoint /usr/local/bin/mutate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/mutate.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### muxbyname.sh
       
```bash
$ singularity exec <container> /usr/local/bin/muxbyname.sh
$ podman run --it --rm --entrypoint /usr/local/bin/muxbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/muxbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### partition.sh
       
```bash
$ singularity exec <container> /usr/local/bin/partition.sh
$ podman run --it --rm --entrypoint /usr/local/bin/partition.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/partition.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### phylip2fasta.sh
       
```bash
$ singularity exec <container> /usr/local/bin/phylip2fasta.sh
$ podman run --it --rm --entrypoint /usr/local/bin/phylip2fasta.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/phylip2fasta.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### pileup.sh
       
```bash
$ singularity exec <container> /usr/local/bin/pileup.sh
$ podman run --it --rm --entrypoint /usr/local/bin/pileup.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/pileup.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### plotflowcell.sh
       
```bash
$ singularity exec <container> /usr/local/bin/plotflowcell.sh
$ podman run --it --rm --entrypoint /usr/local/bin/plotflowcell.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/plotflowcell.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### plotgc.sh
       
```bash
$ singularity exec <container> /usr/local/bin/plotgc.sh
$ podman run --it --rm --entrypoint /usr/local/bin/plotgc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/plotgc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### postfilter.sh
       
```bash
$ singularity exec <container> /usr/local/bin/postfilter.sh
$ podman run --it --rm --entrypoint /usr/local/bin/postfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/postfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### printtime.sh
       
```bash
$ singularity exec <container> /usr/local/bin/printtime.sh
$ podman run --it --rm --entrypoint /usr/local/bin/printtime.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/printtime.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### processfrag.sh
       
```bash
$ singularity exec <container> /usr/local/bin/processfrag.sh
$ podman run --it --rm --entrypoint /usr/local/bin/processfrag.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/processfrag.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### processhi-c.sh
       
```bash
$ singularity exec <container> /usr/local/bin/processhi-c.sh
$ podman run --it --rm --entrypoint /usr/local/bin/processhi-c.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/processhi-c.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### processspeed.sh
       
```bash
$ singularity exec <container> /usr/local/bin/processspeed.sh
$ podman run --it --rm --entrypoint /usr/local/bin/processspeed.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/processspeed.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### randomgenome.sh
       
```bash
$ singularity exec <container> /usr/local/bin/randomgenome.sh
$ podman run --it --rm --entrypoint /usr/local/bin/randomgenome.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/randomgenome.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### randomreads.sh
       
```bash
$ singularity exec <container> /usr/local/bin/randomreads.sh
$ podman run --it --rm --entrypoint /usr/local/bin/randomreads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/randomreads.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### readlength.sh
       
```bash
$ singularity exec <container> /usr/local/bin/readlength.sh
$ podman run --it --rm --entrypoint /usr/local/bin/readlength.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/readlength.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### readqc.sh
       
```bash
$ singularity exec <container> /usr/local/bin/readqc.sh
$ podman run --it --rm --entrypoint /usr/local/bin/readqc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/readqc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### reducesilva.sh
       
```bash
$ singularity exec <container> /usr/local/bin/reducesilva.sh
$ podman run --it --rm --entrypoint /usr/local/bin/reducesilva.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/reducesilva.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### reformat.sh
       
```bash
$ singularity exec <container> /usr/local/bin/reformat.sh
$ podman run --it --rm --entrypoint /usr/local/bin/reformat.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/reformat.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### reformatpb.sh
       
```bash
$ singularity exec <container> /usr/local/bin/reformatpb.sh
$ podman run --it --rm --entrypoint /usr/local/bin/reformatpb.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/reformatpb.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### removebadbarcodes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/removebadbarcodes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/removebadbarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/removebadbarcodes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### removecatdogmousehuman.sh
       
```bash
$ singularity exec <container> /usr/local/bin/removecatdogmousehuman.sh
$ podman run --it --rm --entrypoint /usr/local/bin/removecatdogmousehuman.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/removecatdogmousehuman.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### removehuman.sh
       
```bash
$ singularity exec <container> /usr/local/bin/removehuman.sh
$ podman run --it --rm --entrypoint /usr/local/bin/removehuman.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/removehuman.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### removehuman2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/removehuman2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/removehuman2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/removehuman2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### removemicrobes.sh
       
```bash
$ singularity exec <container> /usr/local/bin/removemicrobes.sh
$ podman run --it --rm --entrypoint /usr/local/bin/removemicrobes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/removemicrobes.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### removesmartbell.sh
       
```bash
$ singularity exec <container> /usr/local/bin/removesmartbell.sh
$ podman run --it --rm --entrypoint /usr/local/bin/removesmartbell.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/removesmartbell.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### rename.sh
       
```bash
$ singularity exec <container> /usr/local/bin/rename.sh
$ podman run --it --rm --entrypoint /usr/local/bin/rename.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/rename.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### renameimg.sh
       
```bash
$ singularity exec <container> /usr/local/bin/renameimg.sh
$ podman run --it --rm --entrypoint /usr/local/bin/renameimg.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/renameimg.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### repair.sh
       
```bash
$ singularity exec <container> /usr/local/bin/repair.sh
$ podman run --it --rm --entrypoint /usr/local/bin/repair.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/repair.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### replaceheaders.sh
       
```bash
$ singularity exec <container> /usr/local/bin/replaceheaders.sh
$ podman run --it --rm --entrypoint /usr/local/bin/replaceheaders.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/replaceheaders.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### representative.sh
       
```bash
$ singularity exec <container> /usr/local/bin/representative.sh
$ podman run --it --rm --entrypoint /usr/local/bin/representative.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/representative.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### rqcfilter.sh
       
```bash
$ singularity exec <container> /usr/local/bin/rqcfilter.sh
$ podman run --it --rm --entrypoint /usr/local/bin/rqcfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/rqcfilter.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### rqcfilter2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/rqcfilter2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/rqcfilter2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/rqcfilter2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### runhmm.sh
       
```bash
$ singularity exec <container> /usr/local/bin/runhmm.sh
$ podman run --it --rm --entrypoint /usr/local/bin/runhmm.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/runhmm.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### samtoroc.sh
       
```bash
$ singularity exec <container> /usr/local/bin/samtoroc.sh
$ podman run --it --rm --entrypoint /usr/local/bin/samtoroc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/samtoroc.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### seal.sh
       
```bash
$ singularity exec <container> /usr/local/bin/seal.sh
$ podman run --it --rm --entrypoint /usr/local/bin/seal.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/seal.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### sendsketch.sh
       
```bash
$ singularity exec <container> /usr/local/bin/sendsketch.sh
$ podman run --it --rm --entrypoint /usr/local/bin/sendsketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/sendsketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### shred.sh
       
```bash
$ singularity exec <container> /usr/local/bin/shred.sh
$ podman run --it --rm --entrypoint /usr/local/bin/shred.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/shred.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### shrinkaccession.sh
       
```bash
$ singularity exec <container> /usr/local/bin/shrinkaccession.sh
$ podman run --it --rm --entrypoint /usr/local/bin/shrinkaccession.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/shrinkaccession.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### shuffle.sh
       
```bash
$ singularity exec <container> /usr/local/bin/shuffle.sh
$ podman run --it --rm --entrypoint /usr/local/bin/shuffle.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/shuffle.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### shuffle2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/shuffle2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/shuffle2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/shuffle2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### sketch.sh
       
```bash
$ singularity exec <container> /usr/local/bin/sketch.sh
$ podman run --it --rm --entrypoint /usr/local/bin/sketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/sketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### sketchblacklist.sh
       
```bash
$ singularity exec <container> /usr/local/bin/sketchblacklist.sh
$ podman run --it --rm --entrypoint /usr/local/bin/sketchblacklist.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/sketchblacklist.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### sketchblacklist2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/sketchblacklist2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/sketchblacklist2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/sketchblacklist2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### sortbyname.sh
       
```bash
$ singularity exec <container> /usr/local/bin/sortbyname.sh
$ podman run --it --rm --entrypoint /usr/local/bin/sortbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/sortbyname.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### splitbytaxa.sh
       
```bash
$ singularity exec <container> /usr/local/bin/splitbytaxa.sh
$ podman run --it --rm --entrypoint /usr/local/bin/splitbytaxa.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/splitbytaxa.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### splitnextera.sh
       
```bash
$ singularity exec <container> /usr/local/bin/splitnextera.sh
$ podman run --it --rm --entrypoint /usr/local/bin/splitnextera.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/splitnextera.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### splitribo.sh
       
```bash
$ singularity exec <container> /usr/local/bin/splitribo.sh
$ podman run --it --rm --entrypoint /usr/local/bin/splitribo.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/splitribo.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### splitsam.sh
       
```bash
$ singularity exec <container> /usr/local/bin/splitsam.sh
$ podman run --it --rm --entrypoint /usr/local/bin/splitsam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/splitsam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### splitsam4way.sh
       
```bash
$ singularity exec <container> /usr/local/bin/splitsam4way.sh
$ podman run --it --rm --entrypoint /usr/local/bin/splitsam4way.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/splitsam4way.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### splitsam6way.sh
       
```bash
$ singularity exec <container> /usr/local/bin/splitsam6way.sh
$ podman run --it --rm --entrypoint /usr/local/bin/splitsam6way.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/splitsam6way.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### stats.sh
       
```bash
$ singularity exec <container> /usr/local/bin/stats.sh
$ podman run --it --rm --entrypoint /usr/local/bin/stats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/stats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### statswrapper.sh
       
```bash
$ singularity exec <container> /usr/local/bin/statswrapper.sh
$ podman run --it --rm --entrypoint /usr/local/bin/statswrapper.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/statswrapper.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### streamsam.sh
       
```bash
$ singularity exec <container> /usr/local/bin/streamsam.sh
$ podman run --it --rm --entrypoint /usr/local/bin/streamsam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/streamsam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### subsketch.sh
       
```bash
$ singularity exec <container> /usr/local/bin/subsketch.sh
$ podman run --it --rm --entrypoint /usr/local/bin/subsketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/subsketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizecontam.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizecontam.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizecontam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizecontam.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizecoverage.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizecoverage.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizecoverage.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizecoverage.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizecrossblock.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizecrossblock.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizecrossblock.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizecrossblock.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizemerge.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizemerge.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizemerge.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizemerge.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizequast.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizequast.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizequast.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizequast.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizescafstats.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizescafstats.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizescafstats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizescafstats.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizeseal.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizeseal.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizeseal.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizeseal.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### summarizesketch.sh
       
```bash
$ singularity exec <container> /usr/local/bin/summarizesketch.sh
$ podman run --it --rm --entrypoint /usr/local/bin/summarizesketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/summarizesketch.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### synthmda.sh
       
```bash
$ singularity exec <container> /usr/local/bin/synthmda.sh
$ podman run --it --rm --entrypoint /usr/local/bin/synthmda.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/synthmda.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### tadpipe.sh
       
```bash
$ singularity exec <container> /usr/local/bin/tadpipe.sh
$ podman run --it --rm --entrypoint /usr/local/bin/tadpipe.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/tadpipe.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### tadpole.sh
       
```bash
$ singularity exec <container> /usr/local/bin/tadpole.sh
$ podman run --it --rm --entrypoint /usr/local/bin/tadpole.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/tadpole.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### tadwrapper.sh
       
```bash
$ singularity exec <container> /usr/local/bin/tadwrapper.sh
$ podman run --it --rm --entrypoint /usr/local/bin/tadwrapper.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/tadwrapper.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### taxonomy.sh
       
```bash
$ singularity exec <container> /usr/local/bin/taxonomy.sh
$ podman run --it --rm --entrypoint /usr/local/bin/taxonomy.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/taxonomy.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### taxserver.sh
       
```bash
$ singularity exec <container> /usr/local/bin/taxserver.sh
$ podman run --it --rm --entrypoint /usr/local/bin/taxserver.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/taxserver.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### taxsize.sh
       
```bash
$ singularity exec <container> /usr/local/bin/taxsize.sh
$ podman run --it --rm --entrypoint /usr/local/bin/taxsize.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/taxsize.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### taxtree.sh
       
```bash
$ singularity exec <container> /usr/local/bin/taxtree.sh
$ podman run --it --rm --entrypoint /usr/local/bin/taxtree.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/taxtree.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### testfilesystem.sh
       
```bash
$ singularity exec <container> /usr/local/bin/testfilesystem.sh
$ podman run --it --rm --entrypoint /usr/local/bin/testfilesystem.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/testfilesystem.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### testformat.sh
       
```bash
$ singularity exec <container> /usr/local/bin/testformat.sh
$ podman run --it --rm --entrypoint /usr/local/bin/testformat.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/testformat.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### testformat2.sh
       
```bash
$ singularity exec <container> /usr/local/bin/testformat2.sh
$ podman run --it --rm --entrypoint /usr/local/bin/testformat2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/testformat2.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### tetramerfreq.sh
       
```bash
$ singularity exec <container> /usr/local/bin/tetramerfreq.sh
$ podman run --it --rm --entrypoint /usr/local/bin/tetramerfreq.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/tetramerfreq.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### textfile.sh
       
```bash
$ singularity exec <container> /usr/local/bin/textfile.sh
$ podman run --it --rm --entrypoint /usr/local/bin/textfile.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/textfile.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### translate6frames.sh
       
```bash
$ singularity exec <container> /usr/local/bin/translate6frames.sh
$ podman run --it --rm --entrypoint /usr/local/bin/translate6frames.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/translate6frames.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### unicode2ascii.sh
       
```bash
$ singularity exec <container> /usr/local/bin/unicode2ascii.sh
$ podman run --it --rm --entrypoint /usr/local/bin/unicode2ascii.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/unicode2ascii.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### unzip.sh
       
```bash
$ singularity exec <container> /usr/local/bin/unzip.sh
$ podman run --it --rm --entrypoint /usr/local/bin/unzip.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/unzip.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### vcf2gff.sh
       
```bash
$ singularity exec <container> /usr/local/bin/vcf2gff.sh
$ podman run --it --rm --entrypoint /usr/local/bin/vcf2gff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/vcf2gff.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```


#### webcheck.sh
       
```bash
$ singularity exec <container> /usr/local/bin/webcheck.sh
$ podman run --it --rm --entrypoint /usr/local/bin/webcheck.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/webcheck.sh   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)