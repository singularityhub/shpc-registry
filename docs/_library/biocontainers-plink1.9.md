---
layout: container
name:  "biocontainers/plink1.9"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/biocontainers/plink1.9/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/biocontainers/plink1.9/container.yaml"
updated_at: "2022-08-18 21:47:04.625770"
latest: "v1.90b6.6-181012-1-deb_cv1"
container_url: "https://hub.docker.com/r/biocontainers/plink1.9"
aliases:
 - "plink"
versions:
 - "v1.90b6.6-181012-1-deb_cv1"
description: "An update to PLINK, a free, open-source whole genome association analysis toolset, designed to perform a range of basic, large-scale analyses in a computationally efficient manner."
config: {"docker": "biocontainers/plink1.9", "latest": {"v1.90b6.6-181012-1-deb_cv1": "sha256:17e280b12dd9101afa2f9a49e8dcf2b1bd2ac34380c21d8e4e677ffbc5dbbe27"}, "tags": {"v1.90b6.6-181012-1-deb_cv1": "sha256:17e280b12dd9101afa2f9a49e8dcf2b1bd2ac34380c21d8e4e677ffbc5dbbe27"}, "filter": ["v*"], "maintainer": "@vsoch", "description": "An update to PLINK, a free, open-source whole genome association analysis toolset, designed to perform a range of basic, large-scale analyses in a computationally efficient manner.", "url": "https://hub.docker.com/r/biocontainers/plink1.9", "aliases": {"plink": "/usr/bin/plink1.9"}}
---

This module is a singularity container wrapper for biocontainers/plink1.9.
An update to PLINK, a free, open-source whole genome association analysis toolset, designed to perform a range of basic, large-scale analyses in a computationally efficient manner.
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install biocontainers/plink1.9
```

Or a specific version:

```bash
$ shpc install biocontainers/plink1.9:v1.90b6.6-181012-1-deb_cv1
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load biocontainers/plink1.9/v1.90b6.6-181012-1-deb_cv1
$ module help biocontainers/plink1.9/v1.90b6.6-181012-1-deb_cv1
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### plink1.9-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### plink1.9-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### plink1.9-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### plink1.9-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### plink1.9-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### plink1.9-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### plink
       
```bash
$ singularity exec <container> /usr/bin/plink1.9
$ podman run --it --rm --entrypoint /usr/bin/plink1.9   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/bin/plink1.9   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)