---
layout: container
name:  "ghcr.io/autamus/fastqc"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/ghcr.io/autamus/fastqc/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/ghcr.io/autamus/fastqc/container.yaml"
updated_at: "2022-08-18 21:47:43.110170"
latest: "0.11.9"
container_url: "https://github.com/orgs/autamus/packages/container/package/fastqc"
aliases:
 - "fastqc"
versions:
 - "0.11.9"
 - "latest"
description: "A quality control tool for high throughput sequence data."
config: {"docker": "ghcr.io/autamus/fastqc", "url": "https://github.com/orgs/autamus/packages/container/package/fastqc", "maintainer": "@vsoch", "description": "A quality control tool for high throughput sequence data.", "latest": {"0.11.9": "sha256:adb7076dea3602571683acec15039079ddf2f7b517669ee39ecaccc368c6a4f3"}, "tags": {"0.11.9": "sha256:adb7076dea3602571683acec15039079ddf2f7b517669ee39ecaccc368c6a4f3", "latest": "sha256:adb7076dea3602571683acec15039079ddf2f7b517669ee39ecaccc368c6a4f3"}, "aliases": {"fastqc": "/opt/view/bin/fastqc"}}
---

This module is a singularity container wrapper for ghcr.io/autamus/fastqc.
A quality control tool for high throughput sequence data.
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install ghcr.io/autamus/fastqc
```

Or a specific version:

```bash
$ shpc install ghcr.io/autamus/fastqc:0.11.9
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load ghcr.io/autamus/fastqc/0.11.9
$ module help ghcr.io/autamus/fastqc/0.11.9
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### fastqc-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### fastqc-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### fastqc-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### fastqc-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### fastqc-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### fastqc-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### fastqc
       
```bash
$ singularity exec <container> /opt/view/bin/fastqc
$ podman run --it --rm --entrypoint /opt/view/bin/fastqc   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /opt/view/bin/fastqc   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)