---
layout: container
name:  "quay.io/biocontainers/bwa"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/quay.io/biocontainers/bwa/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/quay.io/biocontainers/bwa/container.yaml"
updated_at: "2022-08-18 21:46:47.548721"
latest: "0.7.17--h7132678_9"
container_url: "https://quay.io/repository/biocontainers/bwa"
aliases:
 - "bwa"
versions:
 - "0.7.17--h84994c4_4"
 - "0.7.17--h5bf99c6_8"
 - "0.7.17--h7132678_9"
description: "BWA is a program for aligning sequencing reads against a large reference genome."
config: {"docker": "quay.io/biocontainers/bwa", "url": "https://quay.io/repository/biocontainers/bwa", "maintainer": "@marcodelapierre", "description": "BWA is a program for aligning sequencing reads against a large reference genome.", "latest": {"0.7.17--h7132678_9": "sha256:07822e4293a8c59755b295c448b9541db6c9bdbfdedb010bdbdcc1e1e935370f"}, "tags": {"0.7.17--h84994c4_4": "sha256:4f183ae370c240d175cd55424538b39f047c8add50896de896f0d12a73d4a9a0", "0.7.17--h5bf99c6_8": "sha256:f8494324de6da332792dc8e4acc2549152375e1966c96163087d6ff6d42ff48c", "0.7.17--h7132678_9": "sha256:07822e4293a8c59755b295c448b9541db6c9bdbfdedb010bdbdcc1e1e935370f"}, "aliases": {"bwa": "/usr/local/bin/bwa"}}
---

This module is a singularity container wrapper for quay.io/biocontainers/bwa.
BWA is a program for aligning sequencing reads against a large reference genome.
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install quay.io/biocontainers/bwa
```

Or a specific version:

```bash
$ shpc install quay.io/biocontainers/bwa:0.7.17--h84994c4_4
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load quay.io/biocontainers/bwa/0.7.17--h84994c4_4
$ module help quay.io/biocontainers/bwa/0.7.17--h84994c4_4
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### bwa-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### bwa-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### bwa-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### bwa-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### bwa-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### bwa-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### bwa
       
```bash
$ singularity exec <container> /usr/local/bin/bwa
$ podman run --it --rm --entrypoint /usr/local/bin/bwa   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/bwa   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)