---
layout: container
name:  "quay.io/biocontainers/canu"
maintainer: "@vsoch"
github: "https://gitlab.com/singularityhub/shpc-registry/blob/main/quay.io/biocontainers/canu/container.yaml"
config_url: "https://raw.githubusercontent.com/https://gitlab.com/singularityhub/shpc-registry/main/quay.io/biocontainers/canu/container.yaml"
updated_at: "2022-08-18 21:46:50.931967"
latest: "2.2--ha47f30e_0"
container_url: "https://quay.io/repository/biocontainers/canu"
aliases:
 - "canu"
versions:
 - "2.1.1--he1b5a44_0"
 - "2.1.1--he1b5a44_1"
 - "2.2--ha47f30e_0"
 - "2.1.1--h1b792b2_2"
description: "Canu is a fork of the celera assembler designed for high-noise single-molecule sequencing"
config: {"docker": "quay.io/biocontainers/canu", "url": "https://quay.io/repository/biocontainers/canu", "maintainer": "@sarahbeecroft", "description": "Canu is a fork of the celera assembler designed for high-noise single-molecule sequencing", "latest": {"2.2--ha47f30e_0": "sha256:0149e37fd2d15cd60cfe5e3e228604a024d52a62ddd55aee775af7e82a1f4196"}, "tags": {"2.1.1--he1b5a44_0": "sha256:5d986a871985ea20fafcaec02255c61d7136715a05b76db403585a32282ed118", "2.1.1--he1b5a44_1": "sha256:a28f4d1b2289d7f42e47efc0d35ad76ab28cc43a40d2b32aa857eda9c428718d", "2.2--ha47f30e_0": "sha256:0149e37fd2d15cd60cfe5e3e228604a024d52a62ddd55aee775af7e82a1f4196", "2.1.1--h1b792b2_2": "sha256:b48b52afc355477015ef60bebded3b4ab3d3099bbf5698879de8eb600c9ff1a4"}, "aliases": {"canu": "/usr/local/bin/canu"}}
---

This module is a singularity container wrapper for quay.io/biocontainers/canu.
Canu is a fork of the celera assembler designed for high-noise single-molecule sequencing
After [installing shpc](#install) you will want to install this container module:


```bash
$ shpc install quay.io/biocontainers/canu
```

Or a specific version:

```bash
$ shpc install quay.io/biocontainers/canu:2.1.1--he1b5a44_0
```

And then you can tell lmod about your modules folder:

```bash
$ module use ./modules
```

And load the module, and ask for help, or similar.

```bash
$ module load quay.io/biocontainers/canu/2.1.1--he1b5a44_0
$ module help quay.io/biocontainers/canu/2.1.1--he1b5a44_0
```

You can use tab for auto-completion of module names or commands that are provided.

<br>

### Commands

When you install this module, you will be able to load it to make the following commands accessible.
Examples for both Singularity, Podman, and Docker (container technologies supported) are included.

#### canu-run:

```bash
$ singularity run <container>
$ podman run --rm  -v ${PWD} -w ${PWD} <container>
$ docker run --rm  -v ${PWD} -w ${PWD} <container>
```

#### canu-shell:

```bash
$ singularity shell -s /bin/sh <container>
$ podman run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
$ docker run --it --rm --entrypoint /bin/sh  -v ${PWD} -w ${PWD} <container>
```

#### canu-exec:

```bash
$ singularity exec <container> "$@"
$ podman run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
$ docker run --it --rm --entrypoint ""  -v ${PWD} -w ${PWD} <container> "$@"
```

#### canu-inspect:

Podman and Docker only have one inspect type.

```bash
$ podman inspect <container>
$ docker inspect <container>
```

#### canu-inspect-runscript:

```bash
$ singularity inspect -r <container>
```

#### canu-inspect-deffile:

```bash
$ singularity inspect -d <container>
```


#### canu
       
```bash
$ singularity exec <container> /usr/local/bin/canu
$ podman run --it --rm --entrypoint /usr/local/bin/canu   -v ${PWD} -w ${PWD} <container> -c " $@"
$ docker run --it --rm --entrypoint /usr/local/bin/canu   -v ${PWD} -w ${PWD} <container> -c " $@"
```



In the above, the `<container>` directive will reference an actual container provided
by the module, for the version you have chosen to load. An environment file in the
module folder will also be bound. Note that although a container
might provide custom commands, every container exposes unique exec, shell, run, and
inspect aliases. For anycommands above, you can export:

 - SINGULARITY_OPTS: to define custom options for singularity (e.g., --debug)
 - SINGULARITY_COMMAND_OPTS: to define custom options for the command (e.g., -b)
 - PODMAN_OPTS: to define custom options for podman or docker
 - PODMAN_COMMAND_OPTS: to define custom options for the command

<br>
  
### Install

You can install shpc locally (for yourself or your user base) as follows:

```bash
$ git clone https://github.com/singularityhub/singularity-hpc
$ cd singularity-hpc
$ pip install -e .
```

Have any questions, or want to request a new module or version? [ask for help!](https://github.com/singularityhub/singularity-hpc/issues)