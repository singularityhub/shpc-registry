#!/bin/bash

here="$(dirname "${BASH_SOURCE[0]}")"
here="$(realpath "${here}")"
registry=$(dirname ${here})

# rm -rf _library/*
for module in $(shpc show --registry ${registry}); do          
    flatname=${module#/}
    name=$(echo ${flatname//\//-})
    echo "Generating docs for $module, _library/$name.md"
    shpc docgen --registry ${registry} --registry-url https://gitlab.com/singularityhub/shpc-registry $module > "_library/${name}.md"
done
